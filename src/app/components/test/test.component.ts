import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/classes/personne';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  title = 'angular11';
  personne: Personne= new Personne(100, 'Wick');
  constructor(){

  }
  tab: number[] =[2,3,5,8];
  direBonjour(): string {
    return "bonjour Angular";
  }
  personnes: Array<Personne> = [
    new Personne(100, 'Yassmine'),
    new Personne(101, 'Mahdi'),
    new Personne(102, 'Hela'),
    new Personne(103, 'Salma')
  ]

  ngOnInit(): void {
  }

}
